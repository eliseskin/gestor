-- MySQL Script generated by MySQL Workbench
-- Fri Jan 15 19:30:28 2021
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema gestor
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema gestor
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `gestor` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
USE `gestor` ;

-- -----------------------------------------------------
-- Table `gestor`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gestor`.`users` (
  `id` INT NOT NULL,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(250) NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `apellidos` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `gestor`.`categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gestor`.`categoria` (
  `id` INT NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `gestor`.`procesos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gestor`.`procesos` (
  `id` INT NOT NULL,
  `status` INT NOT NULL,
  `proceso` VARCHAR(45) NOT NULL,
  `equipo` INT NOT NULL,
  `date` DATE NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `gestor`.`personal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gestor`.`personal` (
  `id` INT NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `apellidos` VARCHAR(45) NOT NULL,
  `direccion` VARCHAR(45) NOT NULL,
  `telefono` BIGINT(15) NOT NULL,
  `state` INT NOT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


-- -----------------------------------------------------
-- Table `gestor`.`equipo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `gestor`.`equipo` (
  `id` INT NOT NULL,
  `cat_id` INT NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `descripcion` VARCHAR(45) NOT NULL,
  `ns` VARCHAR(45) NOT NULL,
  `url` VARCHAR(250) NOT NULL,
  `timestamp` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

ALTER TABLE `gestor`.`users` 
CHANGE COLUMN `id` `id` INT NOT NULL AUTO_INCREMENT;

ALTER TABLE `gestor`.`categoria` 
CHANGE COLUMN `id` `id` INT NOT NULL AUTO_INCREMENT;

ALTER TABLE `gestor`.`procesos` 
CHANGE COLUMN `id` `id` INT NOT NULL AUTO_INCREMENT;

ALTER TABLE `gestor`.`personal` 
CHANGE COLUMN `id` `id` INT NOT NULL AUTO_INCREMENT;

ALTER TABLE `gestor`.`equipo` 
CHANGE COLUMN `id` `id` INT NOT NULL AUTO_INCREMENT ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
