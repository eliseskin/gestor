const express = require("express");
const morgan = require("morgan");
const path = require("path");
const exphbs = require("express-handlebars");
const session = require("express-session");
const flash = require("connect-flash");
const passport = require("passport");
const MySQLStore = require("express-mysql-session")(session);
const multer = require("multer");
const { database } = require("./config");
// valida campos vacios /[^\s]*$/

const app = express();
require("./lib/passport");

app.set("port", process.env.PORT || 3000);
app.set("views", path.join(__dirname, "views"));
app.engine(
  ".hbs",
  exphbs({
    defaultLayout: "main",
    layoutsDir: path.join(app.get("views"), "layouts"),
    partialsDir: path.join(app.get("views"), "partials"),
    extname: ".hbs",
    helpers: require("./lib/handlebars"),
  })
);
app.set("view engine", ".hbs");

app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const storage = multer.diskStorage({
  destination: path.join(__dirname, "public/upload"),
  filename: (req, file, cb) => {
    cb(null, new Date().getTime() + path.extname(file.originalname));
  },
});

app.use(
  multer({
    storage,
  }).single("image")
);

app.use(
  session({
    secret: "waterreport",
    resave: false,
    saveUninitialized: false,
    store: new MySQLStore(database),
  })
);

app.use(flash());
app.use(passport.initialize());
app.use(passport.session());

app.use((req, res, next) => {
  app.locals.message = req.flash("message");
  app.locals.success = req.flash("success");
  app.locals.user = req.user;
  next();
});

app.use(require("./routes/autentication.routes"));
app.use(require("./routes/index.routes"));
app.use("/main", require("./routes/main.routes"));
app.use("/users", require("./routes/users.routes"));
app.use("/report", require("./routes/reports.routes"));
app.use("/status", require("./routes/status.routes"));
app.use("/personal", require("./routes/personal.routes"));
app.use("/tipo", require("./routes/tipo.routes"));

app.use(express.static(path.join(__dirname, "public")));

app.listen(app.get("port"), () => {
  console.log(`Server on port ${app.get("port")}`);
});
