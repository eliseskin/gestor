const { Router } = require("express");
const { isLoggedIn } = require("../lib/auth");
const pool = require("../database");
const router = Router();

router.get("/", isLoggedIn, async (req, res) => {
  try {
    const reportes = await pool.query("SELECT * FROM equipo");
    const tipo = await pool.query("SELECT * FROM categoria");
    for (let index = 0; index < reportes.length; index++) {
      tipo.forEach((element) => {
        if (element.id == reportes[index].cat_id) {
          reportes[index].cat_id = element.nombre;
        }
      });
    }
    res.render("reports/list", { reportes });
  } catch (error) {
    req.flash("message", "no se pueden cargar los datos equipo");
    res.render("reports/list");
  }
});

router.get("/add", isLoggedIn, async (req, res) => {
  const tipo = await pool.query("SELECT * FROM categoria");
  res.render("reports/add", { tipo });
});

router.post("/add", isLoggedIn, async (req, res) => {
  const { nombre, ns, descripcion, tipo_reporte } = req.body;
  const newReport = {
    cat_id: parseInt(tipo_reporte),
    nombre,
    descripcion,
    ns,
    url: "/upload/" + req.file.filename,
  };
  await pool.query("INSERT INTO equipo set ?", [newReport]);
  req.flash("success", "Equipo Cargado!...");
  res.redirect("/report/add");
});

router.get("/edit/:id", isLoggedIn, async (req, res) => {
  const { id } = req.params;
  let reporte = await pool.query("SELECT * FROM equipo WHERE id = ?", [id]);
  const tipo = await pool.query("SELECT * FROM categoria");
  res.render("reports/edit", { reporte: reporte[0], tipo });
});

router.post("/edit/:id", isLoggedIn, async (req, res) => {
  const { id } = req.params;
  const { nombre, ns, descripcion, tipo_reporte } = req.body;
  const newReport = {
    cat_id: parseInt(tipo_reporte),
    nombre,
    descripcion,
    ns,
    url: "/upload/" + req.file.filename,
  };
  await pool.query("UPDATE equipo set ? WHERE id = ?", [newReport, id]);
  req.flash("success", "Equipo Actualizado!...");
  res.redirect("/report/edit/" + id);
});

router.get("/view/:id", isLoggedIn, async (req, res) => {
  const { id } = req.params;
  try {
    const reporte = await pool.query("SELECT * FROM equipo WHERE id = ?", [id]);
    const tipo = await pool.query("SELECT * FROM categoria");
    const status = await pool.query("SELECT * FROM procesos WHERE equipo = ?", [
      id,
    ]);
    console.log(status);
    tipo.forEach((element) => {
      if (element.id == reporte[0].cat_id) {
        reporte[0].cat_id = element.nombre;
      }
    });
    res.render("reports/view", { reporte: reporte[0], status });
  } catch (error) {
    req.flash("message", "no se pueden cargar los datos");
    res.render("reports/view");
  }
});

router.get("/delete/:id", isLoggedIn, async (req, res) => {
  const { id } = req.params;
  await pool.query("DELETE FROM equipo WHERE id = ?", [id]);
  req.flash("message", "Equipo Eliminado!...");
  res.redirect("/report");
});

module.exports = router;
