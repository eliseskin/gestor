const { Router } = require("express");
const { isLoggedIn } = require("../lib/auth");
const pool = require("../database");

const router = Router();

router.get("/", isLoggedIn, async (req, res) => {
  try {
    const tipo = await pool.query("SELECT * FROM categoria");
    res.render("tipo/list", { tipo });
  } catch (error) {
    req.flash("message", "No se pueden cargar datos categoria");
    res.render("tipo/list");
  }
});

router.get("/add", isLoggedIn, (req, res) => {
  res.render("tipo/add");
});

router.post("/add", isLoggedIn, async (req, res) => {
  const { nombre } = req.body;
  const s = await pool.query("INSERT INTO categoria set ?", [{ nombre }]);
  console.log(s);
  req.flash("success", "Categoria Agregado!");
  res.redirect("/tipo/add");
});

router.get("/edit/:id", isLoggedIn, async (req, res) => {
  const { id } = req.params;
  const tipo = await pool.query("SELECT * FROM categoria WHERE id = ?", [id]);
  res.render("tipo/edit", { tipo: tipo[0] });
});

router.post("/edit/:id", isLoggedIn, async (req, res) => {
  const { id } = req.params;
  const { nombre } = req.body;
  await pool.query("UPDATE categoria set ? WHERE id = ?", [{ nombre }, id]);
  req.flash("success", "Categoria Actualizado!");
  res.redirect("/tipo");
});

router.get("/delete/:id", isLoggedIn, async (req, res) => {
  const { id } = req.params;
  const r = await pool.query("DELETE FROM categoria WHERE id = ?", [id]);
  req.flash("message", "Categoria Eliminado!");
  res.redirect("/tipo");
});

module.exports = router;
