const { Router } = require("express");
const passport = require("passport");
const { isLoggedIn } = require("../lib/auth");

const router = Router();
const pool = require("../database");

router.get("/list", isLoggedIn, async (req, res) => {
  try {
    const users = await pool.query("SELECT * FROM users");
    res.render("users/list", { users });
  } catch (error) {
    req.flash("message", "Error cargar datos");
    res.render("users/list");
  }
});

router.get("/add", isLoggedIn, (req, res) => {
  res.render("users/add");
});

router.get("/delete/:id", isLoggedIn, async (req, res) => {
  const { id } = req.params;
  await pool.query("DELETE FROM users WHERE id = ?", [id]);
  req.flash("success", "Eliminado de la lista");
  res.redirect("/users/list");
});

module.exports = router;
