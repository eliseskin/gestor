const { Router } = require("express");
const pool = require("../database");
const { isLoggedIn } = require("../lib/auth");

const router = Router();

router.get("/dashboard", isLoggedIn, async (req, res) => {
  const equipos = await pool.query("SELECT * FROM equipo");
  const users = await pool.query("SELECT * FROM users");
  const personal = await pool.query("SELECT * FROM personal");
  res.render("principal/dashboard", {
    e: equipos.length,
    u: users.length,
    p: personal.length,
  });
});

module.exports = router;
