const { Router } = require("express");
const { isLoggedIn } = require("../lib/auth");
const pool = require("../database");
const router = Router();

router.get("/add/:id", isLoggedIn, (req, res) => {
  const { id } = req.params;
  res.render("status/add", { id });
});

router.post("/add/:id", isLoggedIn, async (req, res) => {
  const { id } = req.params;
  const { proceso, date } = req.body;
  const newState = {
    status: 1,
    proceso,
    equipo: parseInt(id),
    date,
  };
  await pool.query("INSERT INTO procesos set ?", [newState]);
  req.flash("success", "Proceso Agregado!...");
  res.redirect("/status/add/" + id);
});

router.get("/delete/:id", isLoggedIn, async (req, res) => {
  const { id } = req.params;
  await pool.query("DELETE FROM procesos WHERE id = ?", [id]);
  req.flash("message", "Proceso Eliminado!...");
  res.redirect("/report");
});

module.exports = router;
