const { Router } = require("express");
const passport = require("passport");
const { isLoggedIn, isNotLoggedIn } = require("../lib/auth");

const router = Router();

router.get("/signin", isNotLoggedIn, (req, res) => {
  res.render("auth/signin");
});

router.post("/signin", (req, res, next) => {
  passport.authenticate("local.signin", {
    successRedirect: "/main",
    failureRedirect: "/signin",
    failureFlash: true,
  })(req, res, next);
});

router.get("/logout", (req, res) => {
  req.logOut();
  res.redirect("/signin");
});

router.get("/main", isLoggedIn, (req, res) => {
  res.redirect("/main/dashboard");
});

router.get("/config", (req, res) => {
  res.render("auth/signup");
});

router.post(
  "/config",
  passport.authenticate("local.signup", {
    successRedirect: "/main",
    failureRedirect: "/config",
    failureFlash: true,
  })
);

module.exports = router;
