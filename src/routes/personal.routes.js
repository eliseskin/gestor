const { Router } = require("express");
const { isLoggedIn } = require("../lib/auth");
const router = Router();
const pool = require("../database");

router.get("/list", isLoggedIn, async (req, res) => {
  try {
    const empleados = await pool.query("SELECT * FROM personal");
    res.render("personal/list", { empleados });
  } catch (error) {
    res.render("personal/list");
    req.flash("message", "Error Cargar datos Personal");
  }
});

router.get("/add", isLoggedIn, (req, res) => {
  res.render("personal/add");
});

router.post("/add", isLoggedIn, async (req, res) => {
  const { nombre, apellidos, direccion, telefono, state } = req.body;
  const newPersonal = {
    nombre,
    apellidos,
    direccion,
    telefono: parseInt(telefono),
    state: parseInt(state),
  };
  await pool.query("INSERT INTO personal set ?", [newPersonal]);
  req.flash("success", "Personal Agragado!");
  res.redirect("/personal/add");
});

router.get("/edit/:id", isLoggedIn, async (req, res) => {
  const { id } = req.params;
  const empleado = await pool.query("SELECT * FROM personal WHERE id = ?", [
    id,
  ]);
  res.render("personal/edit", { empleado: empleado[0] });
});

router.post("/edit/:id", isLoggedIn, async (req, res) => {
  const { id } = req.params;
  const { nombre, apellidos, direccion, telefono, state } = req.body;
  const newPersonal = {
    nombre,
    apellidos,
    direccion,
    telefono: parseInt(telefono),
    state: parseInt(state),
  };
  await pool.query("UPDATE personal set ? WHERE id = ?", [newPersonal, id]);
  req.flash("success", "Personal Actualizado!");
  res.redirect("/personal/list");
});

router.get("/delete/:id", isLoggedIn, async (req, res) => {
  const { id } = req.params;
  await pool.query("DELETE FROM personal WHERE id = ?", [id]);
  req.flash("message", "Personal Eliminado!");
  res.redirect("/personal/list");
});

router.get("/view/:id", isLoggedIn, async (req, res) => {
  const { id } = req.params;
  const empleado = await pool.query("SELECT * FROM personal WHERE id = ?", [
    id,
  ]);
  res.render("personal/view", { empleado: empleado[0] });
});

module.exports = router;
